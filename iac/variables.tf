variable "YC_DEFAULT_ZONE" {
   type = string
   default = "ru-central1-b"
}

variable "YC_ACCESS_KEY" {
  type = string
  default = "$YC_ACCESS_KEY"
}

variable "YC_SECRET_KEY" {
  type = string
  default = "$YC_SECRET_KEY"
}

variable "YC_TOKEN" {
  type = string
  default = "$YC_TOKEN"
}
variable "YC_CLOUD_ID" {
  type = string
  default = "$YC_CLOUD_ID"  
}
variable "YC_FOLDER_ID" {
  type = string
  default = "$YC_FOLDER_ID"  
}

variable "name" {
  type = string
  default = "epam-jenkins-yc"
}

variable "cluster_network_name" {
  type = string
  default = "epam-jekins-yc-1"
}

variable "cluster_version" {
  type = string
  default = "1.25"
}

variable "cluster_folder_id" {
  type = string
  default = "$YC_FOLDER_ID"
}
variable "cluster_release_channel" {
  type = string
  default = "RAPID"
}
variable "node_groups_scale" {
  default = {
    service = {
      fixed_scale = 3
    }
    nfs = {
      fixed_scale = 1
    }
    web = {
      auto_scale = {
        max = 3
        min = 3
        initial = 3
      }
    }
  }
}
variable "admin_email" {
  type = string
  default = "nagiev2992@mail.ru"
}
variable "cluster_domain" {
  type = string
  default = "natig2992"
}

variable "admins" {
  type = map(object({
    public_keys = list(string)
  }))
}

variable "output_dir" {
  type = string
  default = "output"
}
