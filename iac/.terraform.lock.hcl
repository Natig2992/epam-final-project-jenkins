# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/kubernetes" {
  version = "2.22.0"
  hashes = [
    "h1:DJr88+52tPK4Ft9xltF6YL+sRz8HWLP2ZOfFiKSB5Dc=",
  ]
}

provider "registry.terraform.io/yandex-cloud/yandex" {
  version     = "0.94.0"
  constraints = "0.94.0"
  hashes = [
    "h1:Sdmw9ZSX1piic70us2/fswomSpcBfDC1IOceadNhbmI=",
  ]
}
