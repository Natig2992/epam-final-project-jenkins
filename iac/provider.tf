terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.94.0"
    }
  }
  #  required_version = ">= 0.13"

backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "tf-state-nat"
    region     = "ru-central1"
    key        = "terraform-nat/terraform.tfstate"
    // access_key = "$YC_ACCESS_KEY"
    // secret_key = "$YC_SECRET_KEY"

    skip_region_validation      = true
    skip_credentials_validation = true

    dynamodb_endpoint = "https://docapi.serverless.yandexcloud.net/ru-central1/b1gpu0552cog1kov6j08/etnnbjdh4f7bnoo7ie2k"
    dynamodb_table    = "terraform-nat"
  }
}

provider "yandex" {
  token     = var.YC_TOKEN
  cloud_id  = var.YC_CLOUD_ID
  folder_id = var.YC_FOLDER_ID
  #service_account_key_file = file("key.json")
  region    = var.YC_DEFAULT_REGION
}

