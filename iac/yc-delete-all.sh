#!/bin/bash

subnet_array=$(yc vpc subnet list | grep epam | cut -d " " -f 2)
echo $subnet_array 

yc vpc subnet delete $subnet_array

yc vpc network delete epam-jenkins-yc

yc container registry delete epam-jenkins-yc 

sa_array=$(yc iam service-account list | grep epam | cut -d " " -f4)
yc iam service-account delete $sa_array
