variable "YC_DEFAULT_ZONE" {
  type = string
  default = "ru-central1-b"
}

variable "YC_TOKEN" {
  type = string
}

variable "YC_CLOUD_ID" {
  type = string
}

variable "YC_FOLDER_ID" {
  type = string
}


variable "registry_name" {
  type = string
}
