resource "yandex_kubernetes_cluster" "cluster" {
  name = var.cluster_network_name

  network_id = var.vpc_id

  master {
    regional {
      region = var.region

      dynamic "location" {
        for_each = var.location_subnets

        content {
          zone = location.value.zone
          subnet_id = location.value.id
        }
      }
    }

    version = var.kube_version
    public_ip = var.public
  }

  service_account_id = var.cluster_service_account_id
  node_service_account_id = var.node_service_account_id

  release_channel = var.release_channel

  depends_on = [
    var.dep
  ]
}

module "node_groups" {
  source = "./modules/node_groups"
  YC_TOKEN = var.YC_TOKEN
  YC_CLOUD_ID = var.YC_CLOUD_ID
  YC_FOLDER_ID = var.YC_FOLDER_ID
  cluster_id = yandex_kubernetes_cluster.cluster.id
  kube_version = var.kube_version
  location_subnets = var.location_subnets
  cluster_node_groups = var.cluster_node_groups
  ssh_keys = var.ssh_keys
}
