variable "YC_DEFAULT_ZONE" {
  type = string
  default = "ru-central1-b"
}

variable "YC_TOKEN" {
  type = string
}

variable "YC_CLOUD_ID" {
  type = string
}

variable "YC_FOLDER_ID" {
  type = string
}

variable "cluster_folder_id" {
  type = string
}
variable "cluster_service_account_name" {
  type = string
}
variable "cluster_node_service_account_name" {
  type = string
}
