variable "YC_DEFAULT_ZONE" {
  type = string
  default = "ru-central1-b"
}

variable "YC_TOKEN" {
  type = string
}

variable "YC_CLOUD_ID" {
  type = string
}

variable "YC_FOLDER_ID" {
  type = string
}

variable "name" {
  type = string
}
variable "subnet" {
  type = string
  default = "10.0.8.0/16"
}
variable "zones" {
  type = list(string)
  default = [
    "ru-central1-a",
    "ru-central1-b",
    "ru-central1-c"]
}
