locals {
  cluster_service_account_name      = "${var.name}-cluster"
  cluster_node_service_account_name = "${var.name}-node"
  
  usernames = keys(var.admins)
  ssh_keys = flatten([
    for admin, config in var.admins: [
      for key in config["public_keys"]: [
        format("%s:%s %s", admin, key, admin)
      ]
    ]
  ])

  cluster_node_group_configs = {
    service = {
      name   = "service"
      cpu    = 6
      memory = 18
      disk = {
        size = 30
        type = "network-ssd"
      }
    }
  }
/*
      nfs = {
      name = "nfs"
      cpu = 2
      memory = 2
      disk = {
        size = 20
        type = "network-ssd"
      }
    }
    web = {
      name = "web"
      cpu = 6
      memory = 12
      disk = {
        size = 64
        type = "network-ssd"
      }
    }
  }
*/
  cluster_node_groups = {
    for key, config in local.cluster_node_group_configs :
    key => merge(config, {
      fixed_scale = lookup(var.node_groups_scale[key], "fixed_scale", false) != false ? [var.node_groups_scale[key].fixed_scale] : []
      auto_scale  = lookup(var.node_groups_scale[key], "auto_scale", false) != false ? [var.node_groups_scale[key].auto_scale] : []
    })
  }
  
  node_selectors = {
    for key, id in module.cluster.node_group_ids:
      key => {
        "yandex.cloud/node-group-id" = id
      }
  }
}


module "vpc" {
  source       = "./modules/vpc"
  YC_FOLDER_ID = var.YC_FOLDER_ID
  YC_CLOUD_ID  = var.YC_CLOUD_ID
  YC_TOKEN     = var.YC_TOKEN
  name         = var.name
}


module "iam" {
  source                            = "./modules/iam"
  YC_TOKEN                          = var.YC_TOKEN
  YC_FOLDER_ID                      = var.YC_FOLDER_ID
  YC_CLOUD_ID                       = var.YC_CLOUD_ID
  cluster_folder_id                 = var.YC_FOLDER_ID
  cluster_service_account_name      = local.cluster_service_account_name
  cluster_node_service_account_name = local.cluster_node_service_account_name
}

module "registry" {
  source        = "./modules/registry"
  YC_TOKEN      = var.YC_TOKEN
  YC_CLOUD_ID   = var.YC_CLOUD_ID
  YC_FOLDER_ID  = var.YC_FOLDER_ID
  registry_name = var.name
}


module "admins" {
  source = "./modules/admins"

  admins = var.admins
  cluster_name = var.name
  cluster_endpoint = module.cluster.external_v4_endpoint
}


module "cluster" {
  source = "./modules/cluster"
  YC_TOKEN                   = var.YC_TOKEN
  YC_FOLDER_ID               = var.YC_FOLDER_ID
  YC_CLOUD_ID                = var.YC_CLOUD_ID
  YC_DEFAULT_ZONE            = var.YC_DEFAULT_ZONE
  cluster_network_name       = var.cluster_network_name
  public                     = true
  kube_version               = var.cluster_version
  release_channel            = var.cluster_release_channel
  vpc_id                     = module.vpc.vpc_id
  location_subnets           = module.vpc.location_subnets
  // location_subnets           = length(var.zones)
  cluster_service_account_id = module.iam.cluster_service_account_id
  node_service_account_id    = module.iam.cluster_node_service_account_id
  cluster_node_groups        = local.cluster_node_groups
  ssh_keys                   = module.admins.ssh_keys
  //ssh_keys                   = local.ssh_keys
  admins                     = var.admins
  dep = [
    module.iam.req
  ]
}

