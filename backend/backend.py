#!/usr/bin/python3
# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=line-too-long
# pylint: disable=method-hidden
# pylint: disable=arguments-renamed
# pylint: disable=trailing-whitespace
# pylint: disable=bad-indentation
# pylint: disable=trailing-newlines
# pylint: disable=invalid-name

from json import JSONEncoder
from datetime import date, datetime
import sys
import os
import logging

import json
import requests
import jsonpickle

from flask import Flask, render_template, request, jsonify
from flask import abort
from flask import make_response
from flask_cors import CORS
from flask_migrate import Migrate
from flask_restful import Api, Resource, reqparse
from prometheus_flask_exporter import PrometheusMetrics
import psycopg2
from psycopg2 import connect, Error




db_credentials = {
   "host": os.getenv('db_host'),
   "user": os.getenv('db_user'),
   "password": os.getenv('db_password'),
   "database": os.getenv('db_database')
}

api_url = "https://itunes.apple.com/search?term=the+beatles&limit=200"
response = requests.get(api_url)
data = response.json()['results']

parsedList = []

def parseJson(_item: dict):

     #print(_item)
    return {'kind': _item.get("kind"), 'collectionName': _item.get("collectionName"), 'trackName': _item.get("trackName"),
            'collectionPrice': _item.get("collectionPrice"), 'trackPrice': _item.get("trackPrice"),
            'releaseDate': _item.get("releaseDate"), 'trackCount': _item.get("trackCount"),
            'trackNumber': _item.get("trackNumber"), 'primaryGenreName': _item.get("primaryGenreName")}



def get_all_data():


    for dictobject in data:
        obj = parseJson(dictobject)
        parsedList.append(obj)

get_all_data()

### Get collectionName for dropmenu ###
def get_collection():

    collection_name = set()
    for collect in parsedList[::]:
        collection_name.add(collect['collectionName'])
    return collection_name


collection_names = get_collection()

def insert_data_db():
    
    db_table = 'beatles'
    # result = []
    try:
        db_conn = psycopg2.connect(**db_credentials)
        curr = db_conn.cursor()
        curr.execute("DROP TABLE IF EXISTS " + db_table)
        curr.execute("CREATE TABLE IF NOT EXISTS " + db_table + " ( kind  varchar(30), collectionName TEXT NOT NULL, trackName TEXT, collectionPrice FLOAT, trackPrice FLOAT, releaseDate timestamptz, trackCount INT, trackNumber INT, primaryGenreName TEXT);")

        for i in parsedList[::]:
            if i['collectionName'] is not None:
                curr.execute("INSERT INTO " + db_table + " (kind, collectionName, trackName, collectionPrice, trackPrice, releaseDate, trackCount, trackNumber, primaryGenreName) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",(i['kind'], i['collectionName'], i['trackName'], i['collectionPrice'], i['trackPrice'], i['releaseDate'], i['trackCount'], i['trackNumber'], i['primaryGenreName']))
        
        db_conn.commit()
        #curr.close
        db_conn.close()
    except psycopg2.Error as postgres_error:
        print("Error connecting to Postgres: " + str(postgres_error))




def main_app():
    insert_data_db()


main_app()

##########################################

app = Flask(__name__)
app.config['WTF_CSRF_ENABLED'] = False # Sensitive

metrics = PrometheusMetrics(app, path=None)
metrics.start_http_server(8000)


@app.route('/backend/api/v1.0/collections', methods=['GET'])
def get_collections():

    global db_table
    db_table = 'beatles'
    try:
        db_conn = psycopg2.connect(**db_credentials)
    except psycopg2.Error as postgres_error:
        print("Error connecting to Postgres: " + str(postgres_error))

    curr = db_conn.cursor()
    curr.execute("SELECT DISTINCT collectionname FROM " + db_table)
    rows_old = curr.fetchall()
    count = 0
    rows = tuple(rows_old)
    row_tup = ()
    for row in rows:
        row_tup += row
        count += 1
    return jsonify(row_tup)



@app.route('/backend/api/v1.0', methods=['GET'])
def all_db():
    try:
        db_conn = psycopg2.connect(**db_credentials)
    except psycopg2.Error as postgres_error:
        print("Error connecting to Postgres: " + str(postgres_error))
    
    db_table = 'beatles'
    curr = db_conn.cursor()
    curr.execute("SELECT DISTINCT * FROM " + db_table + " ORDER BY releasedate ASC;")
    print("The number of songs: ", curr.rowcount)
    rows = curr.fetchall()
    rows_new = jsonify(rows)
    return rows_new


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route('/backend/api/v1.0/update', methods=['GET'])
def update_db():

    get_all_data()
    insert_data_db()    
    db_table = 'beatles'
    
    try:
        db_conn = psycopg2.connect(**db_credentials)
    except psycopg2.Error as postgres_error:
         print("Error connecting to Postgres: " + str(postgres_error))
    
    curr = db_conn.cursor()
    curr.execute("SELECT * FROM " + db_table + " ORDER BY releasedate ASC;")
    print("The number of songs: ", curr.rowcount)
    rows = curr.fetchall()

        
    db_conn.commit()
    #curr.close
    db_conn.close()   
    return jsonify(rows)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port="8080", debug=True)



