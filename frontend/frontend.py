#!/usr/bin/env python3
# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=line-too-long
# pylint: disable=method-hidden
# pylint: disable=arguments-renamed
import os
import logging
from multiprocessing import cpu_count
from multiprocessing import Pool
import signal
import time
import sys
from datetime import datetime
import requests
import psutil

from flask import Flask, render_template
from prometheus_flask_exporter import PrometheusMetrics

sys.path.insert(0, '..')

######## Stress test by CPU ########


def instress(xargs):
    set_time = 1
    timeout = time.time() + 25 * float(set_time)
    while True:
        if time.time() > timeout:
            break


app = Flask(__name__)
app.config['WTF_CSRF_ENABLED'] = False # Sensitive

metrics = PrometheusMetrics(app, path=None)
metrics.start_http_server(8000)

@app.route('/')
@app.route('/index')
def index():
    collections_old = requests.get(os.getenv('backend') + "/backend/api/v1.0/collections")
    collections = list(collections_old.json())
    return render_template("index.html", collections=collections)


@app.route('/collections/<collection>')
def collections(collection):
    rows_old = requests.get(os.getenv('backend') + "/backend/api/v1.0")
    rows = rows_old.json()[::]
    return render_template('collections.html', rows = rows, collection = collection)


@app.route('/update')
def update_info():
    requests.get(os.getenv('backend') + "/backend/api/v1.0/update")
    now = datetime.now()
    timestamp = now.strftime("%m/%d/%Y, %H:%M:%S")
    return render_template('update.html', timestamp=timestamp)


@app.route('/stress')
def stress():
    instress(xargs=1)
    processes = psutil.cpu_count()
    with Pool(processes) as pool:
        pool.map(instress, range(processes))
        return render_template('stress.html')


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
