# ToDo List 

- [x] Write appication (frontend and backend)
- [x] Dockerfile
- [x] Separate front and back
- [X] Database
- [X] Make Stress test

- [x] Orchestration (Kubernetes)
    + [x] Write manifests for apps(frontend and backend)
	+ [x] Write Helm Chart for my app
	+ [x] Manual Deploy manifests and Helm Chart
	+ [x] Letsencrypt, Monitoring and Logging manifests 

- [x] Create GitLab-CI pipline
    + [x] Deploy IaC over ci-cd
	+ [x] Push code to Sonar
	+ [x] QA tests
	+ [x] Build and push container to ECR
	+ [x] Pull from ECR conteiner and deploy via Kubernetes

- [x] Terraform IaC AWS code
    + [x] VPC
	+ [x] RDS
	+ [x] EKS
    + [x] ECR

- [x] Monitoring
- [x] Logging
- [ ] Presentation
